<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="ru"> <!--<![endif]-->

<!-- Mirrored from bestest.kz/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 Feb 2016 05:03:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--<meta name="viewport" content="width=device-width">-->

    <title>Unlimited English - Курсы Английского Языка</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- css -->
    <link rel="stylesheet" href="css/normalizea4f0.css?v=0">
    <link rel="stylesheet" href="css/fontsa4f0.css?v=0">

    <link rel="stylesheet" href="css/main5e1f.css?v=2">
    <link rel="stylesheet" href="css/layout30f4.css?v=3">

    <link rel="stylesheet" href="css/magnific-popupa4f0.css?v=0">
    <script src="/js/jquery-1.12.1.min.js"></script>
    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72079533-1', 'auto');
        ga('require', 'displayfeatures');
        ga('send', 'pageview');

    </script>

    <!-- End of Google Analytics -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        var yaParams = {/*Здесь параметры визита*/};
    </script>

    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter34559070 = new Ya.Metrika({id:34559070,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        trackHash:true,params:window.yaParams||{ }});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="../mc.yandex.ru/watch/34559070/1.gif" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->	</head>
<body>

@include('pages.inc.header')
<!--
    Main
-->
@yield('content')

<!--
    Footer
-->
<footer class="footer clearfix">
    @include('pages.inc.footer')
</footer>

<!--
    Popup
-->
<input type="hidden" name="load" class="load" value="false">
<input type="hidden" name="prefix" class="prefix" value="">
<input type="hidden" name="referer" value="">
<input type="hidden" name="ref_url" value="">
<input type="hidden" name="formname" class="formname" value="">
<input type="hidden" name="source" value="">
<input type="hidden" name="medium" value="">
<input type="hidden" name="campaign" value="">
<input type="hidden" name="content" value="">
<input type="hidden" name="term" value="">

<noindex>
    <!-- Popup Callback -->
    <div class="popup mfp-hide" id="callback" >
        <div class="title">Закажите<br />обратный звонок</div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name" placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <br /><br />
                <button type="submit" class="btn yellow">Оставить заявку</button>
            </form>
        </div>
        <div class="subtitle">
            Мы свяжемся с вами в течение 15 минут
        </div>
    </div>

    <!-- Popup More Info -->
    <div class="popup mfp-hide" id="more-info" >
        <div class="title">Что бы узнать<br />подробнее о курсе</div>
        <div class="subtitle">
            Оставьте заявку, и мы Вам<br />перезвоним в течение 15 минут
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name" placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <br /><br />
                <button type="submit" class="btn yellow">Оставить заявку</button>
            </form>
        </div>
    </div>

    <!-- Popup Reactive IELTS -->
    <div class="popup wide mfp-hide" id="reactive-popup" >
        <div class="title">ЗАПИСАТЬСЯ НА СУПЕР РЕАКТИВНЫЙ IELTS</div>
        <div class="subtitle">
            Оставьте заявку, и мы Вам перезвоним в течение 15 минут
        </div>
        <div class="info">
            ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
            Скидка 44%<br />
            <br /><br />
            IELTS быстро и эффективно<br />
            всего за <span class="price-after">45 118 тг.</span> за курс<br />
            вместо <span class="price-before">81 000 тг</span> за курс<br />
            <br /><br />
            Акция действует до 31 января.
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name"  placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <br /><br />
                <button type="submit" class="noselect btn yellow">Оставить заявку</button>
            </form>
        </div>
        <div class="clear"></div>
    </div>

    <!-- Popup Intensive IELTS -->
    <div class="popup wide mfp-hide" id="intensive-popup" >
        <div class="title">ЗАПИСАТЬСЯ НА ИНТЕНСИВНЫЙ IELTS</div>
        <div class="subtitle">
            Оставьте заявку, и мы Вам перезвоним в течение 15 минут
        </div>
        <div class="info">
            ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
            Скидка 42%<br />
            <br /><br />
            Усиленный IELTS с максимальным прогрессом<br />
            всего за<br />
            <span class="price-after">20 608 тг/мес (61 824 тг за 3 мес.)</span><br />
            вместо<br />
            <span class="price-before">36 000 тг/мес (108 000 тг за 3 мес.)</span><br />
            <br /><br />
            Акция действует до 31 января.
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name"  placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <br /><br />
                <button type="submit" class="noselect btn yellow">Оставить заявку</button>
            </form>
        </div>
        <div class="clear"></div>
    </div>

    <!-- Popup General English -->
    <!--<div class="popup wide mfp-hide" id="general-popup" >
				<div class="title">ЗАПИСАТЬСЯ НА GENERAL ENGLISH</div>
				<div class="subtitle">
					Оставьте заявку, и мы Вам перезвоним в течение 15 минут
				</div>
				<div class="info">
					ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
					Скидка 48%<br />
					<br /><br />
					General English с максимальным прогрессом<br />
					всего за <span class="price-after">14 000 тг</span> в месяц<br />
					вместо <span class="price-before">27 000 тг</span> в месяц<br />
					<br /><br />
					Акция действует до 31 января.
				</div>
				<div class="form-wrapper-std">
					<form>
						<label>ИМЯ</label>
						<input type="text" name="name"  placeholder="Сериков Марат" required />

						<label>ТЕЛЕФОН</label>
						<input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

						<label>ПОЧТА</label>
						<input type="text" name="email" placeholder="marat@example.com" required />

						<br /><br />
						<button type="submit" class="noselect btn yellow">Оставить заявку</button>
					</form>
				</div>
				<div class="clear"></div>
			</div>
-->
    <!-- Popup Business English -->
    <!--<div class="popup wide mfp-hide" id="business-popup" >
				<div class="title">ЗАПИСАТЬСЯ НА BUSINESS ENGLISH</div>
				<div class="subtitle">
					Оставьте заявку, и мы Вам перезвоним в течение 15 минут
				</div>
				<div class="info">
					ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
					Скидка 48%<br />
					<br /><br />
					Business English с максимальным прогрессом<br />
					всего за <span class="price-after">14 000 тг</span> в месяц<br />
					вместо <span class="price-before">27 000 тг</span> в месяц<br />
					<br /><br />
					Акция действует до 31 января.
				</div>
				<div class="form-wrapper-std">
					<form>
						<label>ИМЯ</label>
						<input type="text" name="name"  placeholder="Сериков Марат" required />

						<label>ТЕЛЕФОН</label>
						<input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

						<label>ПОЧТА</label>
						<input type="text" name="email" placeholder="marat@example.com" required />

						<br /><br />
						<button type="submit" class="noselect btn yellow">Оставить заявку</button>
					</form>
				</div>
				<div class="clear"></div>
			</div>
-->
    <!-- Popup English for kids -->
    <div class="popup wide mfp-hide" id="kids-popup" >
        <div class="title">ЗАПИСАТЬСЯ НА ENGLISH FOR KIDS</div>
        <div class="subtitle">
            Оставьте заявку, и мы Вам перезвоним в течение 15 минут
        </div>
        <div class="info">
            ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
            Скидка 54%<br />
            <br /><br />
            English for kids с максимальным прогрессом<br />
            всего за <span class="price-after">14 000 тг</span> в месяц<br />
            вместо <span class="price-before">22 300 тг</span> в месяц<br />
            <br /><br />
            Акция действует до 31 января.
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name"  placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <br /><br />
                <button type="submit" class="noselect btn yellow">Оставить заявку</button>
            </form>
        </div>
        <div class="clear"></div>
    </div>

    <!-- Popup Prepartion English -->
    <div class="popup wide mfp-hide" id="prepartion-popup" >
        <div class="title">ЗАПИСАТЬСЯ</div>
        <div class="subtitle">
            Оставьте заявку, и мы Вам перезвоним в течение 15 минут
        </div>
        <div class="info">
            ВЫГОДНОЕ ПРЕДЛОЖЕНИЕ!<br />
            Скидка 48%<br />
            <br /><br />
            Английский язык для поступления<br />в магистратуру и докторантуру<br /> с максимальным прогрессом<br />
            всего за <span class="price-after">14 000 тг</span> в месяц<br />
            вместо <span class="price-before">27 000 тг</span> в месяц<br />
            <br /><br />
            Акция действует до 31 января.
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name"  placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <br /><br />
                <button type="submit" class="noselect btn yellow">Оставить заявку</button>
            </form>
        </div>
        <div class="clear"></div>
    </div>

    <!-- Popup Question  -->
    <div class="popup mfp-hide" id="popup-question" >
        <div class="title">Задать вопрос</div>
        <div class="subtitle">
            Наш менеджер свяжется<br>
            с Вами в течение 15 мин
        </div>
        <div class="form-wrapper-std">
            <form>
                <label>ИМЯ</label>
                <input type="text" name="name"  placeholder="Сериков Марат" required />

                <label>ТЕЛЕФОН</label>
                <input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />

                <label>ПОЧТА</label>
                <input type="text" name="email" placeholder="marat@example.com" required />

                <label>ВАШ ВОПРОС</label>
                <textarea name="quesion" rows="5" placeholder="?"></textarea>

                <br /><br />
                <button type="submit" class="btn yellow">Оставить заявку</button>
            </form>
        </div>
    </div>

    <!-- Popup Thx  -->
    <div class="popup mfp-hide" id="thx">
        <div class="title">Благодарим Вас!</div>
        <div class="subtitle">
            Мы обязательно перезвоним<br />Вам в ближайшее время
        </div>
    </div>

</noindex>

<!-- Placed at the end of the document so the pages load faster -->

<!-- Custom JS -->
<script src="js/pluginsa4f0.js?v=0"></script>
<script src="js/scripts3860.js?v=1"></script>
<script src="js/smoothscrolla4f0.js?v=0"></script>

<!-- 2gis -->
<script src="../maps.api.2gis.ru/2.0/loaderbf5a.js?pkg=full&amp;lazy=true"></script>
<script type="text/javascript">
    var map;
    DG.then(function () {
        map = DG.map('map', { center: [43.239662, 76.899662], zoom: 16, scrollWheelZoom: false });
        DG.marker([43.239904, 76.902387]).addTo(map).bindPopup('bestest.kz');
    });
</script>

</body>

<!-- Mirrored from bestest.kz/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 01 Feb 2016 05:03:58 GMT -->
</html>
