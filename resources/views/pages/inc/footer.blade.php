<div class="container clearfix">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td class="logo-wrapper">
                <div class="logo">
                    <img src="img/layout/logo-bestest.png" alt="bestest">
                </div>
                <div class="slogan">
                    Learning English easy and quick<br />Making progress week after week
                </div>
            </td>
            <td class="social">
                <div class="social-title">Мы в соц.сетях</div>
                <ul>
                    <li><a class="vk" href="../vk.com/bestestkz.html"></a></li>
                    <li><a class="facebook" href="../www.facebook.com/bestest.html"></a></li>
                    <!-- <li><a class="twitter" href=""></a></li> -->
                    <li><a class="instagram" href="../www.instagram.com/bestest.kz/index.html"></a></li>
                </ul>
            </td>
            <td class="creator">
                <a href="../click2me.kz/indexbfb8.html?utm_source=bestest-footer&amp;utm_medium=banner-logo&amp;utm_campaign=bestest-kz" target="_blank">
                    <div class="social-title">Разработано в</div>
                </a>
            </td>
        </tr>
    </table>
</div>