
<!--
    Navigation
-->
<div class="nav-wrapper">
    <div class="nav">
        <div class="container clearfix">
            <nav>
                <ul class="nav-main-list">
                    <!-- <li><a href="#features">Почему мы</a></li> -->
                    <li><a href="#standard">Standard</a></li>
                    <li><a href="#premium">Premium</a></li>
                    <li><a href="#unlimited">Unlimited</a></li>
                    <li><a href="#free-ielts">Акция «Бесплатный IELTS»</a></li>
                    <li><a href="#team">Наши преподаватели</a></li>
                    <li><a href="#contacts">Контакты</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<!--
    Header
-->
<header class="header clearfix">
    <div class="container">
        <div class="logo-wrapper">
            <div class="logo">
                <!-- <img src="img/layout/logo-bestest.png" alt="bestest"> -->
                Unlimited English
            </div>
            <!-- <div class="slogan">
                Learning English easy and quick<br />Making progress week after week
            </div> -->
        </div>
        <div class="phone">
            <div class="phone-number">+7 (777) 77-777-77<br /> +7 (707) 707-77-77 </div>
            <div class="red-link magnificPopup btn-popup" href="#callback" data-formname="Бесплатный звонок">
                Заказать бесплатный звонок
            </div>
        </div>
    </div>
</header>