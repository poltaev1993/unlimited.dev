@extends('layout')
@section('content')
    <section class="view main" id="main">
        <div class="container clearfix">
            <div class="img-wrapper">
                <img src="img/layout/main-offer-brain.png" alt="Курс IELTS light" width="300" />

            </div>
            <div class="title">Unlimited English</div>
            <div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
            <!-- <div class="price-before">за 2000 тг/час</div>
            <div class="price-after">от 1000 тг/час</div> -->
            <br />
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#more-info" data-formname="Подробнее о курсе">
                    УЗНАТЬ ПОДРОБНЕЕ
                </div>
            </div>
            <!-- <div class="timer left">
                <div class="head">До конца акции осталось:</div>
                <div class="timer_block">
                    <div class="day_wrapp">
                        <div class="count day">00</div>
                        <div class="timer_desc">дня</div>
                    </div>
                    <div class="hour_wrapp">
                        <div class="count hour">00</div>
                        <div class="timer_desc">часов</div>
                    </div>
                    <div class="min_wrapp">
                        <div class="count min">00</div>
                        <div class="timer_desc">минут</div>
                    </div>
                    <div class="sec_wrapp">
                        <div class="count sec">00</div>
                        <div class="timer_desc">секунд</div>
                    </div>
                </div>
            </div> -->
            <div class="finish-date">Предложение действует только до 31 января!</div>
        </div>
    </section>

    <!--
        Features
    -->
    <!-- <section class="view features" id="features">
        <div class="container clearfix">
            <div class="title">У нас есть всё, что нужно вам нужно:</div>
            <div class="feature result">
                <div class="icon"></div>
                <div class="name">ГАРАНТИРОВАННЫЙ<br />РЕЗУЛЬТАТ</div>
                <div class="desc">Вы cдадите IELTS<br />как минимум на 6.0<br />за 6 недель</div>
            </div>
            <div class="feature exp">
                <div class="icon"></div>
                <div class="name">ОПЫТНЫЕ<br />ПРЕПОДАВАТЕЛИ</div>
                <div class="desc">Раскроют все тайны<br />быстрой подготовки<br />к IELTS</div>
            </div>
            <div class="feature price">
                <div class="icon"></div>
                <div class="name">САМЫЕ НИЗКИЕ<br />ЦЕНЫ В АЛМАТЫ</div>
                <div class="desc">Всего 836 тенге<br />за 1 час</div>
            </div>
            <div class="feature free">
                <div class="icon"></div>
                <div class="name">ВОЗМОЖНОСТЬ СДАТЬ<br />IELTS БЕСПЛАТНО</div>
                <div class="desc">Покажи лучший<br />результат</div>
            </div>
            <div class="subtitle">А ещё:</div>
            <div class="feature break">
                <div class="icon"></div>
                <div class="name">СЛАДКИЙ<br />ПЕРЕРЫВ</div>
                <div class="desc">Шоколадки, печеньки,<br />конфеты к чаю<br />в любое время</div>
            </div>
            <div class="feature motiv">
                <div class="icon"></div>
                <div class="name">СУПЕР ТЕХНИКА<br />МОТИВАЦИИ</div>
                <div class="desc">Эффективная подготовка<br />с «волшебными пинками»</div>
            </div>
            <div class="feature club">
                <div class="icon"></div>
                <div class="name">ENGLISH CLUB КАЖДУЮ<br />НЕДЕЛЮ БЕСПЛАТНО</div>
                <div class="desc">Вас ждут интересные<br />развлечения</div>
            </div>
        </div>
    </section> -->

    <!--
        Programms
    -->
    <section class="view programm" id="programm">
        <div class="container clearfix">
            <div class="title">Программы обучения</div>
            <div class="subtitle">Курс IELTS light включает 3 программы, если:</div>

            {{--Begin grid--}}
            <div class="grid clearfix">
                <div class="grid-1-3">
                    <div class="c_grid">
                        <div class="name">Srandard</div>
                        <div class="icon">
                            <img src="img/layout/programm-rocket.png" alt="Ракета"/>
                        </div>
                        <div class="btn-wrapper">
                            <div class="btn red magnificPopup btn-popup" href="#reactive-popup" data-formname="Реактивный IELTS">
                                ЗАПИСАТЬСЯ НА КУРС
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid-1-3">
                    <div class="c_grid">
                        <div class="name">Premium</div>
                        <div class="icon">
                            <img src="img/layout/premium.png" alt="Воздушный шар"/>
                        </div>
                        <div class="btn-wrapper">
                            <div class="btn red magnificPopup btn-popup" href="#reactive-popup" data-formname="Реактивный IELTS">
                                ЗАПИСАТЬСЯ НА КУРС
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid-1-3">
                    <div class="c_grid">
                        <div class="name" style="text-align:center;margin-bottom: 20px;">Unlimited</div>
                        <div class="icon">
                            <img src="img/layout/programm-balloon.png" alt="Ракета"/>
                        </div>
                        <div class="btn-wrapper">
                            <div class="btn red magnificPopup btn-popup" href="#reactive-popup" data-formname="Реактивный IELTS">
                                ЗАПИСАТЬСЯ НА КУРС
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--End grid--}}
        </div>
        </div>
    </section>

    <!--
        Reactive IELTS
    -->
    <section class="view reactive" id="standard">
        <div class="container clearfix">
            <div class="recommend">Рекомендуемый</div>
            <br />
            <div class="course-title">Standard</div>
            <br />
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        6 недель подготовки
                    </li>
                    <li>
                        54 академических часа (<span class="line-through">1500 тг/час</span> 836 тг/час)
                    </li>
                    <li>
                        Бесплатное тестирование IELTS для определения<br />
                        уровня каждую неделю по 3 часа
                    </li>
                    <li>
                        2 часа в день, 3 раза в неделю
                    </li>
                    <li>
                        Ускоренная программа обучения по специальной<br />
                        методике
                    </li>
                    <li>
                        Ориентированность на практическую часть
                    </li>
                </ul>
            </div>
            <div class="course-price-before">81 000 тг.</div>
            <div class="course-price-after right">
                <div class="info">Всего за</div>
                <div class="count">45 118 тг.</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#reactive-popup" data-formname="Реактивный IELTS">
                    ЗАПИСАТЬСЯ
                </div>
            </div>
        </div>
    </section>

    <!--
        Intensive IELTS
    -->
    <section class="view intensive" id="premium">
        <div class="container clearfix">
            <div class="course-title">Premium</div>
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        3 месяца подготовки
                    </li>
                    <li>
                        72 академических часа (<span class="line-through">1500 тг/час</span> 859 тг/час)
                    </li>
                    <li>
                        Бесплатное тестирование для определения уровня каждую<br />
                        неделю 3 часа
                    </li>
                    <li>
                        1,5 часа в день, 2 раза в неделю
                    </li>
                    <li>
                        Поэтапная программа обучения с подробным объяснением<br />
                        деталей
                    </li>
                    <li>
                        Ориентированность на практику и на теорию одновременно
                    </li>
                </ul>
            </div>
            <div class="course-price-before">
                36 000 тг/мес<br />108 000 тг за 3 мес.
            </div>
            <div class="course-price-after left">
                <div class="info">Всего за</div>
                <div class="count">20 608 тг/мес<br />61 824 тг за 3 мес.</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#intensive-popup" data-formname="Интенсивный IELTS">
                    ЗАПИСАТЬСЯ
                </div>
            </div>
        </div>
    </section>

    <section class="view reactive" id="unlimited">
        <div class="container clearfix">
            <div class="recommend">Рекомендуемый</div>
            <br />
            <div class="course-title">Unlimited</div>
            <br />
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        6 недель подготовки
                    </li>
                    <li>
                        54 академических часа (<span class="line-through">1500 тг/час</span> 836 тг/час)
                    </li>
                    <li>
                        Бесплатное тестирование IELTS для определения<br />
                        уровня каждую неделю по 3 часа
                    </li>
                    <li>
                        2 часа в день, 3 раза в неделю
                    </li>
                    <li>
                        Ускоренная программа обучения по специальной<br />
                        методике
                    </li>
                    <li>
                        Ориентированность на практическую часть
                    </li>
                </ul>
            </div>
            <div class="course-price-before">81 000 тг.</div>
            <div class="course-price-after right">
                <div class="info">Всего за</div>
                <div class="count">45 118 тг.</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#reactive-popup" data-formname="Реактивный IELTS">
                    ЗАПИСАТЬСЯ
                </div>
            </div>
        </div>
    </section>

    <!--
        Free IELTS
    -->
    <section class="view free-ielts" id="free-ielts">
        <div class="container clearfix">
            <div class="title">Сдай пробный IELTS бесплатно</div>
            <div class="info">
                <div class="subtitle">
                    Мы предоставляем уникальную возможность<br />сдать пробный IELTS  бесплатно!
                </div>
                <br />
                <div class="description">
                    Каждую субботу в течение дня Вы можете прийти на экзамен.<br />
                    Для этого оставьте заявку и запишитесь на пробный IELTS.<br />
                    Чтобы узнать более подробные условия акции, оставьте<br />
                    заявку или позвоните нам.<br />
                </div>
                <br />
                <div class="finish-date">Акция только до 31 марта!</div>
                <div class="timer right">
                    <div class="head">До конца акции осталось:</div>
                    <div class="timer_block">
                        <div class="day_wrapp">
                            <div class="count day">00</div>
                            <div class="timer_desc">дня</div>
                        </div>
                        <div class="hour_wrapp">
                            <div class="count hour">00</div>
                            <div class="timer_desc">часов</div>
                        </div>
                        <div class="min_wrapp">
                            <div class="count min">00</div>
                            <div class="timer_desc">минут</div>
                        </div>
                        <div class="sec_wrapp">
                            <div class="count sec">00</div>
                            <div class="timer_desc">секунд</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-wrapper-std">
                <div class="form-title">
                    Оставьте заявку, чтобы участвовать<br />в акции «Сдай пробный IELTS бесплатно»
                </div>
                {!! Form::open(['url' => '/leave-request']) !!}
                    {!!  Form::label('name', 'ИМЯ') !!}
                    {!!  Form::text('name', null, ['placeholder' => 'Имя...', 'required' => 'true']) !!}

                    {!!  Form::label('surname', 'Фамилия') !!}
                    {!!  Form::text('surname', null, ['placeholder' => 'Фамилия...', 'required' => 'true']) !!}

                    {!!  Form::label('phone', 'ТЕЛЕФОН') !!}
                    {!!  Form::text('phone', null, ['class' => 'input_phone', 'placeholder' => 'Введите Ваш номер...', 'required' => 'true']) !!}

                    {!!  Form::label('email', null, 'ПОЧТА') !!}
                    {!!  Form::text('email', null, ['class' => 'input_phone', 'placeholder' => 'Введите Ваш Email...', 'required' => 'true']) !!}

                    {!!  Form::submit('Оставить заявку', ['class' => 'btn yellow', 'data-formname' => "заявка"]) !!}
                {!!  Form::close() !!}
            </div>
        </div>
    </section>

    <!--
        General
    -->
    <section class="view general" id="general">
        <div class="container clearfix">
            <div class="course-title">General English</div>
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        Продолжительность курса от 3 до 9 месяцев.
                    </li>
                    <li>
                        3 раза в неделю по 1 часу.
                    </li>
                    <li>
                        Данный курс подходит, если вы хотите:<br />
                        &nbsp;- уверенно общаться, понимать речь на английском языке;<br />
                        &nbsp;- работать в международной компании;<br />
                        &nbsp;- путешествовать в англоязычных странах;<br />
                        &nbsp;- читать литературу на английском;<br />
                        &nbsp;- сдавать экзамены по английскому языку на высокие баллы.
                    </li>
                </ul>
            </div>
            <div class="course-price-before">
                27 000 тг/мес
            </div>
            <div class="course-price-after right">
                <div class="info">Всего за</div>
                <div class="count">14 000 тг/мес</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#general-popup" data-formname="General English">
                    ЗАПИСАТЬСЯ НА<br />GENERAL ENGLISH
                </div>
            </div>
        </div>
    </section>

    <!--
        Business
    -->
    <!--<section class="view business" id="business">
        <div class="container clearfix">
            <div class="course-title">Business English</div>
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        Данный курс поможет овладеть лексикой и терминологией<br />
                        деловой тематики и повысить уровень профессионализма.
                    </li>
                    <li>
                        Продолжительность курса от 1 до 9 месяцев.
                    </li>
                    <li>
                        3 раза в неделю по 1 часу.
                    </li>
                    <li>
                        Подходит для всех уровней.
                    </li>
                    <li>
                        Данный курс подходит, если вы:<br />
                        &nbsp;- бизнесмен или сотрудник международной компании;<br />
                        &nbsp;- хотите вести переговоры, переписку, беседы, встречи<br />
                        &nbsp;&nbsp;и презентации с зарубежными партнёрами;<br />
                        &nbsp;- хотите улучшить навыки общения именно в деловой сфере.
                    </li>
                </ul>
            </div>
            <div class="course-price-before">
                27 000 тг/мес
            </div>
            <div class="course-price-after left">
                <div class="info">Всего за</div>
                <div class="count">14 000 тг/мес</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#business-popup" data-formname="Business English">
                    ЗАПИСАТЬСЯ НА<br />BUSINESS ENGLISH
                </div>
            </div>
        </div>
    </section>
    -->
    <!--
        Kids
    -->
    <!--<section class="view kids" id="kids">
        <div class="container clearfix">
            <div class="course-title">English for kids</div>
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        Обучение детей с 7 до 12 лет.
                    </li>
                    <li>
                        Продолжительность курса от 1 до 9 месяцев.
                    </li>
                    <li>
                        3 раза в неделю по 1 часу.
                    </li>
                    <li>
                        Подходит для всех уровней.
                    </li>
                    <li>
                        Изучение английского языка в игровой манере<br />
                        &nbsp;с увлекательными темами и интересными сюжетами.<br />
                        &nbsp;Ваш ребёнок:<br />
                        &nbsp;- научится уверенно общаться;<br />
                        &nbsp;- будет правильно строить предложения;<br />
                        &nbsp;- правильно произносить слова;<br />
                        &nbsp;- повысит успеваемость в школе по английскому языку;<br />
                        &nbsp;- полюбит английский.<br />
                        &nbsp;Всё это поможет вашему ребёнку достигнуть своих<br />
                        &nbsp;целей во взрослой жизни, где требуется знание<br />
                        &nbsp;английского языка.
                    </li>
                </ul>
            </div>
            <div class="course-price-before">
                22 300 тг/мес
            </div>
            <div class="course-price-after right">
                <div class="info">Всего за</div>
                <div class="count">14 000 тг/мес</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#kids-popup" data-formname="English for kids">
                    ЗАПИСАТЬСЯ НА<br />ENGLISH FOR KIDS
                </div>
            </div>
        </div>
    </section>-->

    <!--
        Preparation
    -->
    <!--<section class="view preparation" id="preparation">
        <div class="container clearfix">
            <div class="course-title">Английский язык для поступления<br />в магистратуру и докторантуру</div>
            <div class="ul-wrapper">
                <ul class="red-square">
                    <li>
                        Данный курс поможет сдать вступительный экзамен<br />
                        &nbsp;по английскому языку на высокие баллы в магистратуру<br />
                        &nbsp;и докторантуру в ВУЗы Казахстана.
                    </li>
                    <li>
                        Продолжительность курса от 1 до 9 месяцев.
                    </li>
                    <li>
                        3 раза в неделю по 1,5 часа.
                    </li>
                    <li>
                        Подходит для всех уровней.<br />
                        &nbsp;Вы научитесь супер техникам, которые помогут Вам легко,<br />
                        &nbsp;быстро, а, главное, успешно сдать экзамен с максимальными<br />
                        &nbsp;баллами.
                    </li>
                </ul>
            </div>
            <div class="course-price-before">
                27 000 тг/мес
            </div>
            <div class="course-price-after left">
                <div class="info">Всего за</div>
                <div class="count">14 000 тг/мес</div>
            </div>
            <div class="clear"></div>
            <div class="btn-wrapper">
                <div class="btn yellow magnificPopup btn-popup" href="#prepartion-popup" data-formname="Preparation English">
                    ЗАПИСАТЬСЯ
                </div>
            </div>
        </div>
    </section>
    -->
    <!--
        Certificates
    -->
    <section class="view certificate" id="certificate">
        <div class="container clearfix">
        </div>
    </section>

    <!--
        IELTS Light
    -->
    <!--<section class="view light" id="light">
			<div class="container clearfix">
				<div class="title">Акция</div>
				<div class="info">
					<div class="subtitle">
						Скидки от 40% на любую программу<br />курса "IELTS light"
					</div>
					<br />
					<div class="description">
						Успейте записаться по низким ценам!
						<br /><br /><br />
					</div>
					<br />
					<div class="finish-date">Акция только до 31 января!</div>
					<div class="timer right">
						<div class="head">До конца акции осталось:</div>
						<div class="timer_block">
							<div class="day_wrapp">
								<div class="count day">00</div>
								<div class="timer_desc">дня</div>
							</div>
							<div class="hour_wrapp">
								<div class="count hour">00</div>
								<div class="timer_desc">часов</div>
							</div>
							<div class="min_wrapp">
								<div class="count min">00</div>
								<div class="timer_desc">минут</div>
							</div>
							<div class="sec_wrapp">
								<div class="count sec">00</div>
								<div class="timer_desc">секунд</div>
							</div>
						</div>
					</div>
					<div class="discount">от 40%<br />скидка</div>
				</div>
				<div class="form-wrapper-std">
					<div class="form-title">
						Оставьте заявку, чтобы пройти<br />обучение со скидкой
					</div>
					<form>
						<label>ИМЯ</label>
						<input type="text" name="name" placeholder="Сериков Марат" required />
						<label>ТЕЛЕФОН</label>
						<input type="text" name="phone" class="input_phone" placeholder="+7 (123) 456-78-95" required />
						<label>ПОЧТА</label>
						<input type="text" name="email" placeholder="marat@example.com" required />
						<button type="submit" class="btn yellow" data-formname="Обучение со скидкой">ПОЛУЧИТЬ СКИДКУ</button>
					</form>
				</div>
			</div>
		</section>
-->
    <!--
        Review
    -->
    <section class="view review" id="review">
        <div class="container clearfix">
        </div>
    </section>

    <!--
        Team
    -->
    <!--<section class="view team" id="team">
        <div class="container clearfix">
            <div class="title">Наши Преподаватели</div>
            <div class="staff">
                <img src="img/team/staff-1.jpg" alt="Abdugani">
                <div class="info">
                    <div class="name">ABDUGANI BAIDILDAYEV</div>
                    <div class="pos">Teacher of English</div>
                    <div class="uni">American University<br />in Bulgaria</div>
                </div>
            </div>
            <div class="staff">
                <img src="img/team/staff-2.jpg" alt="Aitolkyn">
                <div class="info">
                    <div class="name">AITOLKYN KALDYORAZ</div>
                    <div class="pos">Teacher of English</div>
                    <div class="uni">Kazakh-British<br />Technical University</div>
                </div>
            </div>
            <div class="staff">
                <img src="img/team/staff-3.jpg" alt="Amir">
                <div class="info">
                    <div class="name">AMIR KURMANGAZIN</div>
                    <div class="pos">Teacher of English</div>
                    <div class="uni">Kazakh-British<br />Technical University</div>
                </div>
            </div>
        </div>
    </section>
    -->
    <!--
        Contacts
    -->
    <section class="view contacts" id="contacts">
        {{--<div class="container clearfix">
            <div class="title">Ответим на любой ваш вопрос</div>
            <div class="link magnificPopup btn-popup" href="#popup-question" data-formname="Задайте нам вопрос">
                Закажите звонок специалиста
            </div>
            <div class="subtitle">или позвоните по телефонам</div>
            <span class="phone">+ 7 (777) 77-777-77</span><span class="phone">|</span><span class="phone"> +7 (777) 777-77-77</span>
        </div>--}}
        <div id="map" class="map clearfix">
            <div class="info">
                <div class="label1">Мы находимся</div>
                <div>г.Алматы, ул. Сатпаева,<br />уг. ул. Байзакова,<br />2 этаж, "Бизнес фабрика"</div>
                <div class="label">ТЕЛЕФОН</div>
                <div>+ 7 (702) 77-777-77<br /> +7 (707) 707-77-77 </div>
                <div class="label">EMAIL ДЛЯ ЗАЯВОК</div>
                <div>some@gmail.com</div>
            </div>
        </div>
    </section>
@stop