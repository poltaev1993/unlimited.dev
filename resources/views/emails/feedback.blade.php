<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
</head>
<body>
<h2 align="center">Вам пришло сообщение с сайта Unlimited English .</h2>
<div align="center" style="font-size:18px">
    <p>
        Прислал(a) вам это сообщение {{ $userRequests->name }}
    </p>
    <p>
        E-mail:  {{ $userRequests->email }}.
    </p>
    <p>
        Контактный номер: {{$userRequests->phone}}.
    </p>
</div>
</body>
</html>