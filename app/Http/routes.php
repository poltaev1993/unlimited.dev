<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin'], function(){
    Route::get('login', 'Admin\AdminController@getLogin');
    Route::post('login', 'Admin\AdminController@postLogin');

    Route::group(['middleware' => 'auth'], function(){
        Route::get('logout', 'Admin\AdminController@logout');
        Route::controller('/', 'Admin\AdminController');
    });
});
Route::controller('/', 'PageController');
