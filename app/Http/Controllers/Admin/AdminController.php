<?php

namespace App\Http\Controllers\Admin;

use App\RequestModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('admin.index');
    }

    public function getLogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect('admin');
        } else {
            return redirect()->back()->withInput()->with('auth-error', 'Неправильные данные для входа');
        }
    }

    public function getRequests(){
        $requests = RequestModel::all();
        return view('admin.pages.requests', compact('requests'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('admin');
    }
}
