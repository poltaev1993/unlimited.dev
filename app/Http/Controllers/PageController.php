<?php

namespace App\Http\Controllers;

use App\RequestModel;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('pages.index');
    }

    public function postLeaveRequest(Request $request)
    {
        RequestModel::create($request->all());

        Mail::send('emails.feedback', ['userRequests' => $request], function ($m){
            $m->from('unlim.english.school@gmail.com', 'Unlimited English');
            $m->to(['poltaev1993@gmail.com', 'unlim.english.school@gmail.com'])->subject('Заявки');
        });
        return redirect()->back()->with('success', 'Заявка была успешно отправлена!');
    }
}
