<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestModel extends Model
{

    protected $table = 'request';

    protected $fillable = [
        'name',
        'surname',
        'phone',
        'email',
    ];
}
