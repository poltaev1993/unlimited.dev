$(document).ready(function(){

	"use strict";

//------------------------------------------------------------------------
//						LAZY IMAGE SETTINGS
//------------------------------------------------------------------------
$(document).ready(function(){
    SpeedUpHandler();
});

var SpeedUpHandler = function() {
    $(".lazy").lazy({
        effect          : "fadeIn",
        effectTime      : 500,        // if you want some effect you have to specify a duration
        combined        : true,
        delay           : 12500,
        scrollDirection : 'vertical',
        visibleOnly     : false,      // could be removed, it's default value
        threshold       : 2 * $(window).height(),
        defaultImage    : "",         // this is a bug, that the default image is set to non-images
                                      // i'll updated this within the next version
        afterLoad: function(element)
        {
            element.removeClass("loading").addClass("loaded");
        }
    });
}



//------------------------------------------------------------------------
//						MASKED INPUT SETTINGS
//------------------------------------------------------------------------

$(".input_phone").mask("+7(999)999-99-99",{placeholder:" "});



//------------------------------------------------------------------------
//						GO TO TOP SETTINGS
//------------------------------------------------------------------------

$(window).scroll(function () {
    if ($(this).scrollTop() > 400) {
	   $('.go-to-top').fadeIn();
    } else {
	   $('.go-to-top').fadeOut();
    }
	$('.go-to-top').smoothScroll({
		speed: 900,
		scrollTarget: 'body',
		easing: 'swing'
	});
});


//------------------------------------------------------------------------
//						STELLAR (PARALLAX) SETTINGS
//------------------------------------------------------------------------

if(!(navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i))) {
	$.stellar({
		horizontalScrolling: false,
		positionProperty: 'transform'
	});
}



//------------------------------------------------------------------------
//						WOW ANIMATION SETTINGS
//------------------------------------------------------------------------

var wow = new WOW({
	offset:100,        // distance to the element when triggering the animation (default is 0)
	mobile:false       // trigger animations on mobile devices (default is true)
});
wow.init();

//------------------------------------------------------------------------
//						SCROLL BOTTOM SETTINGS
//------------------------------------------------------------------------
$(".scroll-bottom").click(function() {
    $('html, body').animate({
        scrollTop: $("#service").offset().top-90
    }, 1000, 'swing');
});

//------------------------------------------------------------------------
//						TIMER SETTINGS
//------------------------------------------------------------------------

timer(); // Call Timer Function

function timer(){
	var now = new Date();
	//var newDate = new Date((now.getMonth()+1)+"/"+now.getDate()+"/"+now.getFullYear()+" 23:59:59");
	var newDate = new Date("Jan 31 2016 23:59:00");
	var totalRemains = (newDate.getTime()-now.getTime());
	if (totalRemains>1)
	{
		var Days = (parseInt(parseInt(totalRemains/1000)/(24*3600)));
		var Hours = (parseInt((parseInt(totalRemains/1000) - Days*24*3600)/3600));
		var Min = (parseInt(parseInt((parseInt(totalRemains/1000) - Days*24*3600) - Hours*3600)/60));
		var Sec = parseInt((parseInt(totalRemains/1000) - Days*24*3600) - Hours*3600) - Min*60;
		if (Days<10){Days="0"+Days}
		if (Hours<10){Hours="0"+Hours}
		if (Min<10){Min="0"+Min}
		if (Sec<10){Sec="0"+Sec}
		$(".day").each(function() { $(this).text(Days); });
		$(".hour").each(function() { $(this).text(Hours); });
		$(".min").each(function() { $(this).text(Min); });
		$(".sec").each(function() { $(this).text(Sec); });
		setTimeout(timer, 1000);
	}
}



//------------------------------------------------------------------------
//                    MAGNIFIC POPUP(LIGHTBOX) SETTINGS
//------------------------------------------------------------------------


$(document).ready(function(){

	$('.portfolio__item').click(function(){
		$('a[href="#popup-iframe"]').click();
		$('#popup-iframe').children().eq(0).attr('src',$(this).data('href'));
		$('#popup-iframe').css('margin-top','3%').parent().css('height','90%').css('top','0');
	});

});
$('.magnificPopup').magnificPopup({
	type:'inline',
	midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
});

var groups = {};
$('.magnificImage').each(function() {
  var id = parseInt($(this).attr('data-group'), 30);

  if(!groups[id]) {
    groups[id] = [];
  }

  groups[id].push( this );
});

$.each(groups, function() {

  $(this).magnificPopup({
	type: 'image',
	mainClass: 'mfp-zoom-in',
	tLoading: '',
	gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	},
	removalDelay: 500, //delay removal by X to allow out-animation
	callbacks: {

	imageLoadComplete: function() {
		var self = this;
		setTimeout(function() {
			self.wrap.addClass('mfp-image-loaded');
		}, 16);
	},
		close: function() {
		this.wrap.removeClass('mfp-image-loaded');
	},


	// don't add this part, it's just to avoid caching of image
	beforeChange: function() {
	 this.items[0].src = this.items[0].src + '?=' + Math.random();
	}
  },

  closeBtnInside: false,
  closeOnContentClick: true,
  midClick: true
  })

});

// Add it after jquery.magnific-popup.js and before first initialization code
$.extend(true, $.magnificPopup.defaults, {
	tClose: 'Закрыть (Esc)', // Alt text on close button
	tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
	gallery: {
		tPrev: 'Предыдущий', // Alt text on left arrow
		tNext: 'Следующий)', // Alt text on right arrow
		tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
  },
	image: {
    tError: '<a href="%url%">Изоображение</a> не может быть загружено.' // Error message when image could not be loaded
  },
	ajax: {
    tError: '<a href="%url%">Контент</a> пустой.' // Error message when ajax request failed
  }
});


//------------------------------------------------------------------------
//						THANK YOU POPUP SETTINGS
//------------------------------------------------------------------------
function thx(id) {
	$.magnificPopup.open({
		items: {
			src: id,
			type: 'inline'
		}
	}, 0);
	$('input[type="text"]').each(function(){
		$(this).val('');
	});
	$('textarea').val('');
}	
	


/*!
**************************
*  Hovered Navigation http://codetheory.in/change-active-state-links-sticky-navigation-scroll/
**************************
*/
$(document).ready(function () {

	var sections = $('section')
	  , nav = $('nav')
	  , nav_height = nav.outerHeight();

	$(window).on('scroll', function () {
	  var cur_pos = $(this).scrollTop();

	  sections.each(function() {
		var top = $(this).offset().top - nav_height,
			bottom = top + $(this).outerHeight();

		if (cur_pos >= top && cur_pos <= bottom) {
		  nav.find('a').removeClass('active');
		  sections.removeClass('active');

		  $(this).addClass('active');
		  nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
		}
	  });
	});

	nav.find('a').on('click', function () {
	  var $el = $(this)
		, id = $el.attr('href');

	  $('html, body').animate({
		scrollTop: $(id).offset().top - 60
	  }, 500);

	  return false;
	});

});

//------------------------------------------------------------------------
//						DONT LEAVE POPUP SETTINGS
//------------------------------------------------------------------------

/*var popupactivated = 0;

setTimeout( function() {
	addEvent(document, "mouseout", function(e) {
		e = e ? e : window.event;
		var from = e.relatedTarget || e.toElement;
		if (!from || from.nodeName == "HTML") {
			if(popupactivated == 0) {
				$('a[href="#popup-plus"]').click();
				popupactivated = 1;
			}
		}
	});
} , 7000);
*/
	
	
	
//------------------------------------------------------------------------
//						FORM ACTIONS SETTINGS
//------------------------------------------------------------------------

var prefix = $('.prefix').val();
var url = prefix+"mail.php";
var mobile = navigator.userAgent.toLowerCase().match(/(iphone|ipod|ipad|android)/);

$('.openpopup').click(function() {
	href = $(this).attr("href");
	formname = $(this).attr("data-formname");
	$('.formname').attr("value", formname);
	$.magnificPopup.open({
		items: {
			src: href,
			type: 'inline',
		}
	}, 0);
});
$('.btn-popup').click(function() {
	formname = $(this).attr("data-formname");
	if (formname) {
		$('.formname').attr("value", formname);
	}
	$(this).parents("form").submit();
	return false;
});
$('.btn.close').click(function() {
	$.magnificPopup.close();
});
$('form').submit(function() {
	$('body').find('form:not(this)').children('label').removeClass('red');
	var request_url = '\n'+$('input[name="ref_url"]').val().toString().replace(/&/g, '\n');
	var $form = $(this);
	var answer = checkForm($form.get(0));
	if(answer != false)
	{
		var name = $('input[name="name"]', $form).val(),
			phone = $('input[name="phone"]', $form).val(),
			email = $('input[name="email"]', $form).val(),
			ques = $('textarea[name="ques"]', $form).val(),
			sbt = $('.btn', $form).attr("data-name"),
			submit = $('.btn', $form).text();
			//submit = $('input[name='+sbt+']', $form).val();

		var	source = $('input[name="source"]').val();
		var	medium = $('input[name="medium"]').val();
		var	campaign = $('input[name="campaign"]').val();
		var	content = $('input[name="content"]').val();
		var	term = $('input[name="term"]').val();
		var	ref = $('input[name="referer"]').val();
		var formname = $('input[name="formname"]').val();
		var data = "source="+source+"&medium="+medium+"&campaign="+campaign+"&content="+content+"&term="+term+"&"+sbt+"="+submit+"&formname="+formname+"&ref="+ref;
		data += "&" + $form.serialize();
		$.ajax({
			type: 'POST',
			url: url,
			dataType: 'json',
			data: data,
			success: function(msg){

			},
			error: function(){

			}
		}).always(function() {
			//метрики
			ga('send', 'event', 'knopka', 'lead');
			yaCounter34559070.reachGoal('zayavka');
			thx("#thx");

		});
	}
	return false;
});


//------------------------------------------------------------------------
//						VALIDATOR SETTINGS
//------------------------------------------------------------------------
function checkForm(form1) {

  var $form = $(form1),
      checker = true,
      name = $("input.name", $form).val(),
      phone = $("input.phone", $form).val(),
      email = $("input.email", $form).val();

  if($form.find(".name").hasClass("required")) {
    if(!name) {
      $form.find(".name").addClass("red");
      checker = false;
    } else {
      $form.find(".name").removeClass('red');
    }
  }
 if($form.find(".phone").hasClass("required")) {
    if(!phone) {
      $form.find(".phone").addClass("red");
      checker = false;
    } else {
      $form.find(".phone").removeClass('red');
    }
  }
  if($form.find(".email").hasClass("required")) {
    if(!email) {
      $form.find(".email").addClass("red");
      checker = false;
    } else if(!/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/.test(email)) {
      $form.find(".email").addClass("red");
      checker = false;
    } else {
      $form.find(".email").removeClass("red");
    }
  }

  if(checker != true) { return false; }
}

//------------------------------------------------------------------------
//						FORMNAME SETTINGS
//------------------------------------------------------------------------
function formname(name) { //onClick="formname('text');"
	$('.formname').attr("value", name);
	console.log("formname = " + name);
}
function targets(name) { //onClick="formname('text');"
	$('.target').attr("value", name);
	console.log("target = " + name);
}

	
	
});



//------------------------------------------------------------------------
//						PORTFOLIO GRID SETTINGS
//------------------------------------------------------------------------

gridGenerate();

function addEvent(obj, evt, fn) {
	if (obj.addEventListener) {
		obj.addEventListener(evt, fn, false);
	}
	else if (obj.attachEvent) {
		obj.attachEvent("on" + evt, fn);
	}
}

gridGenerate();
function gridGenerate() {
	var arr = [
		['Soile Kalam', '#', '3D РУЧКА:', '1', 'http://soile-kalam.kz'],
		['Bentoncosmetic', '#', 'Натуральная корейская косметика для всех', '2', 'http://bentoncosmetic.kz'],
		['Paminakids', '#', 'Модные платья для девочек, напрямую от производителя!', '3', 'http://paminakids.kz/'],
		['Тамирлан', '#', 'Продажа люстр, бра в Астане по оптовым ценам', '4', 'http://lustra-shop.kz'],
		['Mini-Moika', '#', 'Домашняя автомойка', '5', 'http://mini-moika.kz/'],
		['Орнек', '#', 'ЭКСКЛЮЗИВНЫЕ КАЗАХСКИЕ ЮВЕЛИРНЫЕ УКРАШЕНИЯ И СУВЕНИРЫ', '6', 'http://orneksouvenirs.kz/shop/'],
		['DiMebel', '#', 'Мебельная фурнитура', '7', 'http://dimebel.kz/'],
		['БалБобек', '#', 'ДЕТСКИЙ САД И ЯСЛИ В МЕДЕУСКОМ РАЙОНЕ.', '8', 'http://balbobek.kz'],
		['Электронды Жайнамаз', '#', 'Электронды Жайнамаз', '9', 'http://zhainamaz.kz'],
	];
	arr = shuffle(arr);
	var i = 0;
	$('.portfolio__item').each(function(){
		$(this).css({'background-image':'url(img/portfolio/'+arr[i][3]+'.jpg)'});
		$(this).children('.portfolio__item__title').html(arr[i][0]);
		$(this).children('.portfolio__item__subtitle').html(arr[i][2]);
		$(this).attr('data-href',arr[i][4]);
		i++;
	});

	var port_arr = new Array($('.portfolio > .container > .portfolio-grid').length-1);

	for(l = 0; l < $('.portfolio > .container > .portfolio-grid').length; l++) {
		port_arr[l] =	$('.portfolio > .container > .portfolio-grid').eq(l).html($('.portfolio > .container > .portfolio-grid').eq(l).clone()).html();
	}

	$('.portfolio > .container').empty();

	shuffle(port_arr);

	for(n = 0; n < port_arr.length; n++) {
		$('.portfolio > .container').append(port_arr[n]);
	}

}

function shuffle(array) {
	for(var j, x, i = array.length; i; j = parseInt(Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x);
	return array;
}